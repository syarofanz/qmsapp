import React, { Component } from 'react';
import { Image, View, StyleSheet, Dimensions, TouchableHighlight, FlatList, PermissionsAndroid } from 'react-native';
import { Container, Content, Card, CardItem, Body, Text, Thumbnail, Input, Form, Item, Label, Button } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import RNHTMLtoPDF from 'react-native-html-to-pdf';

const getwidth = Dimensions.get("window").width;
const getHeight = Dimensions.get("window").height;

const dataList = [
	{
		id: '1',
		title: 'First Item',
		desc: 'Description First Item',
	},
	{
		id: '2',
		title: 'Second Item',
		desc: 'Description Second Item',
	},
	{
		id: '3',
		title: 'Third Item',
		desc: 'Description Third Item',
	},
];

class Antrian extends Component {
	constructor(props) {
		super(props);
		this.state = {
			bgcolor : '#dce8fa',
			dataList : [],
			filePath : ''
		}
		this.getDataFromApi();
	}

	// async storeData(){
	// 	try {
	// 		const address = this.state.address;
	// 		await AsyncStorage.setItem('url', address);
	// 		this.getData();
	// 	} catch (e) {
	// 		console.log(e);
	// 	}
	// }

	// async getData(){
	// 	console.log('satu');
	// 	try {
	// 		const value = await AsyncStorage.getItem('url')
	// 		this.setState({
	// 			url : value,
	// 			bgcolor : '#dce8fa'
	// 		});
	// 	} catch(e) {
	// 		console.log(e);
	// 	}
	// }

	askPermission(data) {
		var that = this;
		console.log('minta ijin');
		async function requestExternalWritePermission() {
			try {
				const granted = await PermissionsAndroid.request(
					PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
					{
						title: 'QMSApp External Storage Write Permission',
						message:
						'QMSApp needs access to Storage data in your SD Card ',
					}
					);
				if (granted === PermissionsAndroid.RESULTS.GRANTED) {
					that.createPDF(data);
				} else {
				alert('WRITE_EXTERNAL_STORAGE permission denied');
				}
			} catch (err) {
				alert('Write permission err', err);
			console.warn(err);
			}
		}
		//Calling the External Write permission function
		if (Platform.OS === 'android') {
			requestExternalWritePermission();
		} else {
			this.createPDF(data);
		}
	}

	async createPDF(data) {
		// let data = data.split(" : ");
		console.log(data);
		let options = {
			html : '<div id="judulpage" style="width:100%; overflow:auto; padding:15px; font-size:15pt; font-family:Arial Black" align="center"><strong>'+data[0]+'</strong></div><div id="judulpage" style="width:100%; overflow:auto; padding:15px; font-size:60pt; font-family:Arial Black" align="center"><strong>'+data[1]+'</strong></div><div id="judulpage" style="width:100%; overflow:auto; padding:10px; font-size:10pt; font-family:Arial Black" align="center">'+data[2]+'</div>',
			//File Name
			fileName: 'Print Antrian '+data[0]+' '+data[1]+' '+data[2],
			//File directory
			directory: 'QMSApp',
		};

		let file = await RNHTMLtoPDF.convert(options);
		console.log(file.filePath);
		this.setState({filePath:file.filePath});
		alert('File saved on : '+file.filePath);
	}

	async getDataFromApi(){
		try {
			const data	= await axios.get('http://36.67.103.243:3367/devel/index.php?mod=ant_awal&cmd=api_kiostombol&akid=1');
			let dataList = data.data.data;
			dataList = Object.keys(dataList).map(key => ({
				[0]: dataList[key]
			}));
			this.setState({ dataList });
		}catch(e){
			console.log(e);
		}
	}

	async getNoAntrian(aktid){
		try {
			let data	= await axios.get('http://36.67.103.243:3367/devel/index.php?mod=ant_awal&cmd=act_klik&ajax=t&aktid='+aktid);
			data 		= data.data[1].antrian;
			// let data = "LOKET 1 : A2 : 11 Desember 2019";
			console.log(data);
			if(data != ''){
				data = data.split(" : ");
				// alert(data[0]+"\nNo Antrian : "+data[1]+"\nJam : "+data[2]);
				this.askPermission(data);
			}else{
				alert("No Data");
			}
		}catch(e){
			console.log(e);
		}
	}

	itemLeft(item){
		let data	= item;
		let gg		= data.id % 2;
		return(
			<View>
				{gg == 1 ? 
					<TouchableHighlight onPress={() => {
						this.getNoAntrian(data.aktid);
					}}>
						<Card>
							<CardItem>
								<View style={styles.wrapper}>
									<View style={styles.container}>
										<View style={ styles.imageLeft }>
											<Thumbnail square large source={ require('./../assets/images/print.png') } />
										</View>
										<View style={ styles.contentLeft }>
											<Text style={ styles.title }>
												{data.atname}
											</Text>
											<Text style={ styles.description }>
												{data.sub_antrian}
											</Text>
										</View>
									</View>
							  </View>
							</CardItem>
						</Card>
					</TouchableHighlight>

					:

					<TouchableHighlight onPress={() => {
						this.getNoAntrian(data.aktid);
					}}>
						<Card>
							<CardItem>
								<View style={styles.wrapper}>
									<View style={styles.container}>
										<View style={ styles.contentRight }>
											<Text style={ styles.title }>
												{data.atname}
											</Text>
											<Text style={ styles.description }>
												{data.sub_antrian}
											</Text>
										</View>
										<View style={ styles.imageRight }>
											<Thumbnail square large source={ require('./../assets/images/print.png') } />
										</View>
									</View>
							  </View>
							</CardItem>
						</Card>
					</TouchableHighlight>
				}
			</View>
			
		)
	}

	renderContent(){
		return(
			<FlatList
				data={this.state.dataList}
				renderItem={(item) => {
					let data = item.item[0];
					return(
						this.itemLeft(data)
					)
				}}
				keyExtractor={(item, index) => index.toString()}
				// keyExtractor={item => item.id}
			/>
		);
	}

	render() {
		return (
			<Container>
				<Content style={{ padding : 5, backgroundColor : this.state.bgcolor }}>
					<View>
						{this.renderContent()}
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	wrapper: {
		flex: 1
	},
	container: {
		flexDirection: 'row',
		justifyContent: 'flex-start',
	},
	buttonStyle : {
		marginLeft : 10,
		marginRight : 10
	},
	imageLeft: {
		flex : 0.3,
		backgroundColor: '#2196F3',
		alignItems : 'center',
		justifyContent: 'center',
		paddingTop : 15,
		paddingBottom : 15,
	},
	contentLeft: {
		flex : 0.7,
		marginLeft : 8
	},
	imageRight: {
		flex : 0.3,
		backgroundColor: '#2196F3',
		alignItems : 'center',
		justifyContent: 'center',
		paddingTop : 15,
		paddingBottom : 15,
	},
	contentRight: {
		flex : 0.7,
		marginRight : 8
	},
	title : {
		fontSize: 22,
		fontWeight: 'bold',
		color : '#475669',
		textShadowColor: '#ccc',
		textShadowOffset: {width: -1, height: 1},
		textShadowRadius: 5
	},
	description : {
		color : '#9AA6B6',
		fontSize: 15,
		textShadowColor: '#ccc',
		textShadowOffset: {width: -1, height: 1},
		textShadowRadius: 5
	}
});

module.exports = Antrian;